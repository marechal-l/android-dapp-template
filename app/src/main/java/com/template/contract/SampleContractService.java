package com.template.contract;

import com.template.contract.exception.ContractNotInitialized;
import com.template.network.FutureCallback;
import com.template.network.WaitTransaction;
import com.template.network.Web3jNetworkManager;

import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.ClientTransactionManager;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;
import org.web3j.tx.ReadonlyTransactionManager;

import java.io.IOException;
import java.math.BigInteger;

public class SampleContractService {

    private static SampleContractService instance;

    public static SampleContractService getInstance(){
        if(instance == null){
            instance = new SampleContractService();
        }
        return instance;
    }

    private SampleContractService(){}


    private SampleContract contract;

    public void loadContract(Credentials credentials){
        Web3j web3j = Web3jNetworkManager.getInstance().getWeb3j();
        contract = SampleContract.load(
                SampleContract.getPreviouslyDeployedAddress("4"),
                web3j,
                credentials,
                ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
    }

    public boolean isValid() throws IOException {
        return contract.isValid();
    }

    public void incrementId(FutureCallback callback) throws Exception{
        if(contract == null){
            throw new ContractNotInitialized();
        }
        WaitTransaction.waitForFuture(contract.incrementId().sendAsync(), callback);
    }

    public void getId(FutureCallback callback) throws Exception{
        if(contract == null) {
            throw new ContractNotInitialized();
        }
        WaitTransaction.waitForFuture(contract.getId().sendAsync(), callback);
    }
}
