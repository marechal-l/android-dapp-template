package com.template.contract.exception;

public class ContractNotInitialized extends Exception {

    public ContractNotInitialized(String s){
        super(s);
    }

    public ContractNotInitialized(){
        this("ContractNotInitialized");
    }
}
