package com.template.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.template.application.WalletManager;
import com.template.contract.SampleContractService;
import com.template.lmarechal.android_dapp_template.R;
import com.template.network.FutureCallback;
import com.template.network.Web3jNetworkManager;

import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.Web3jFactory;

import java.util.concurrent.Future;


public class MainActivity extends AppCompatActivity {

    public static final String TAG = "DAPP_TEMPLATE_LOGS";

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //This is my public token for infura, please use your own if you want to use this app :)
        Web3jNetworkManager.getInstance().setWeb3j(Web3jFactory.build(new HttpService("https://rinkeby.infura.io/v3/4045218c7f17466ab58eeaccab83d2fd")));
        SampleContractService.getInstance().loadContract(WalletManager.getInstance(getApplicationContext()).getCredentials());

        textView = (TextView) findViewById(R.id.viewID);

        findViewById(R.id.getId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SampleContractService.getInstance().getId(new FutureCallback() {
                        @Override
                        public void isDone(Future<?> future) {
                            try {

                                String id = (future.get()).toString();
                                Log.d(TAG, "id : " + id);
                                textView.setText(id);
                            }catch (Exception e) {
                                Log.e(TAG, "onGetId callback : " + e.getMessage());
                            }
                        }
                    });
                } catch (Exception e) {
                    Log.e(TAG, "onGetId : " + e.getMessage());
                }
            }
        });

        findViewById(R.id.incrementId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SampleContractService.getInstance().incrementId(new FutureCallback() {
                        @Override
                        public void isDone(Future<?> future) {
                            try{
                                String message = "incrementId is done : " + ((TransactionReceipt)future.get()).getStatus();
                                Log.d(TAG, message);
                                textView.setText(message);
                            }catch (Exception e) {
                                Log.e(TAG, "incrementId callback : " + e.getMessage());
                            }
                        }
                    });
                } catch (Exception e) {
                    Log.e(TAG, "onIncrementId : " + e.getMessage());
                }
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_profile:
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
