package com.template.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.template.application.WalletManager;
import com.template.lmarechal.android_dapp_template.R;

import org.web3j.crypto.CipherException;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity {

    private WalletManager walletManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        walletManager = WalletManager.getInstance(getApplicationContext());
    }

    protected void onResume(){
        super.onResume();
        ((EditText)findViewById(R.id.etPassword)).setText("");
    }


    public void onLogin(View v){
        String password = ((EditText)findViewById(R.id.etPassword)).getText().toString();
        if(password == null || password.isEmpty()){
            ((TextView)findViewById(R.id.tvError)).setText("Password can't be empty");
            return;
        }
        try{
            if(walletManager.loadCredentials(password)){
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            }
        }catch(IOException e) {
            ((TextView)findViewById(R.id.tvError)).setText("Wallet file not found");
        }catch (CipherException e) {
            ((TextView)findViewById(R.id.tvError)).setText("Wrong password");
        }
    }

    public void onImport(View v){
        Intent intent = new Intent(this, ImportActivity.class);
        startActivity(intent);
    }
}
