package com.template.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.template.application.WalletManager;
import com.template.lmarechal.android_dapp_template.R;
import com.template.network.Web3jNetworkManager;

import org.web3j.crypto.Credentials;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.utils.Convert;

import java.math.BigDecimal;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);

        Credentials credentials = WalletManager.getInstance(getApplicationContext()).getCredentials();
        if(credentials != null){
            String network = "Rinkeby";
            String address = credentials.getAddress();
            String tokenCurrency = "ETH";
            String tokenNb = "00.00";
            String moneyCurrency = "Dollars";
            String moneyValue = "00.00";

            try {
                EthGetBalance ethGetBalance = Web3jNetworkManager.getInstance().getWeb3j()
                        .ethGetBalance(address, DefaultBlockParameterName.LATEST)
                        .sendAsync()
                        .get();
                BigDecimal nbETH = Convert.fromWei(ethGetBalance.getBalance().toString(), Convert.Unit.ETHER);
                tokenNb = nbETH.toString();

            } catch (Exception e){
                Log.e(MainActivity.TAG, "" + e.getMessage());
            }


            ((TextView)findViewById(R.id.tvNetwork)).setText(network);
            ((TextView)findViewById(R.id.tvAddress)).setText(address);
            ((TextView)findViewById(R.id.tvTokenCurrency)).setText(tokenCurrency);
            ((TextView)findViewById(R.id.tvTokenNb)).setText(tokenNb);
            ((TextView)findViewById(R.id.tvMoneyCurrency)).setText(moneyCurrency);
            ((TextView)findViewById(R.id.tvMoneyValue)).setText(moneyValue);
        }else{
            AlertDialog alertDialog = new AlertDialog.Builder(ProfileActivity.this).create();
            alertDialog.setTitle("Wallet issue");
            alertDialog.setMessage("You are not logged in!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            redirectToLogin();

                        }
                    });
            alertDialog.show();
        }
    }

    private void redirectToLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
