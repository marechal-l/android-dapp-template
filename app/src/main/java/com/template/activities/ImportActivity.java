package com.template.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.template.application.WalletManager;
import com.template.lmarechal.android_dapp_template.R;

import org.web3j.crypto.CipherException;

import java.io.IOException;

public class ImportActivity extends AppCompatActivity {

    private WalletManager walletManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.import_activity);
        walletManager = WalletManager.getInstance(getApplicationContext());
    }

    protected void onResume(){
        super.onResume();
        ((EditText)findViewById(R.id.etPassword)).setText("");
        ((EditText)findViewById(R.id.etPvKey)).setText("");
    }

    public void onImport(View v){
        try{
            String password = ((EditText)findViewById(R.id.etPassword)).getText().toString();
            String privateKey = ((EditText)findViewById(R.id.etPvKey)).getText().toString();

            if(password == null || privateKey == null || password.isEmpty() || privateKey.isEmpty()){
                ((TextView)findViewById(R.id.tvError)).setText("The fields cannot be empty");
                return;
            }

            if(walletManager.importWallet(privateKey, password)){
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            }
        }catch (CipherException e){
            ((TextView)findViewById(R.id.tvError)).setText("Cipher error");
        }catch (IOException e){
            ((TextView)findViewById(R.id.tvError)).setText("IO error");
        }
    }
}
