package com.template.network;

import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;

public class Web3jNetworkManager {

    private static Web3jNetworkManager instance;

    public static Web3jNetworkManager getInstance(){
        if(instance == null){
            instance = new Web3jNetworkManager();
        }
        return instance;
    }

    private Web3jNetworkManager(){}


    private Web3j web3j;

    public void setWeb3j(Web3j o){this.web3j = o;}
    public Web3j getWeb3j(){return web3j;}
}
