package com.template.network;

import android.util.Log;

import java.util.concurrent.Future;

import static com.template.activities.MainActivity.TAG;

public class WaitTransaction<T> {

    public static <T> void waitForFuture(final Future<T> future, final FutureCallback callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    do {
                        Thread.sleep(1000);
                        Log.d(TAG, "Wait transaction");
                    } while (!future.isDone());
                    callback.isDone(future);
                } catch (Exception e) {
                    Log.e(TAG, "onTransfer : " + e.getMessage());
                }
            }
        }).start();
    }
}
