package com.template.network;

import java.util.concurrent.Future;

public interface FutureCallback {

    void isDone(Future<?> future);

}
