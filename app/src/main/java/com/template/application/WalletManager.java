package com.template.application;

import android.content.Context;

import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;

import java.io.File;
import java.io.IOException;

public class WalletManager {

    private static WalletManager instance;

    public static WalletManager getInstance(Context context){
        if(instance == null){
            instance = new WalletManager(context);
        }
        return instance;
    }

    private WalletManager(Context context){
        destination = context.getFilesDir().getAbsolutePath();
    }

    private String destination;
    private Credentials credentials;

    public boolean loadCredentials(String password) throws CipherException, IOException {
        File directory = new File(destination);
        File[] files = directory.listFiles();
        if(files.length == 0){
            throw new IOException();
        }
        credentials = WalletUtils.loadCredentials(password, files[0]);
        return credentials != null;
    }

    public boolean importWallet(String privateKey, String password) throws CipherException, IOException {
        Credentials credentials = Credentials.create(privateKey);
        File directory = new File(destination);
        clearDirectory(directory);
        WalletUtils.generateWalletFile(password, credentials.getEcKeyPair(), directory, false);
        this.credentials = credentials;
        return true;
    }

    private void clearDirectory(File directory){
        String[] children = directory.list();
        for (int i = 0; i < children.length; i++) {
            new File(directory, children[i]).delete();
        }
    }

    public Credentials getCredentials(){return credentials;}
}
